package com.how2java.tmall.service;

import com.how2java.tmall.pojo.Category;
import com.how2java.tmall.util.Page;

import java.util.List;

/**
 * @author YLxia
 */
public interface CategoryService {
    /**
     * 查询
     * @param page 分页信息
     * @return List<Category>
     */
    List<Category> list(Page page);

    /**
     * 计算总数
     * @return int
     */
    int total();
}