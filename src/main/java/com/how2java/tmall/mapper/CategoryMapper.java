package com.how2java.tmall.mapper;

import com.how2java.tmall.pojo.Category;
import com.how2java.tmall.util.Page;

import java.util.List;

/**
 * @author YLxia
 */
public interface CategoryMapper {
    /**
     * 查询
     * @param page 分页信息
     * @return List<Category>
     */
    List<Category> list(Page page);

    /**
     * 获取总数
     * @return int
     */
    int total();
}
